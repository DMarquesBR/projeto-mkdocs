# Nathália e Diego


Músicas que gostamos:

- [Poesia acústica]
- [Linkin Park](https://www.youtube.com/watch?v=eVTXPUF4Oz4)
- [Menos é Mais](https://www.youtube.com/watch?v=I5Rl73wFmWs)




## Cartinha de Amor

    Meu Amor, saiba que estarei sempre do 
    seu lado, nos seus dias mais pilhada 
    e nos dias mais desanimada.
    
    Saiba que eu vou respeitar seu espaço,
    mas sempre vou tentar de colocar pra 
    cima e te fazer mais feliz.

    !!! AMO VOCÊ !!!


[Poesia acústica]: <https://www.youtube.com/watch?v=FMWiJwLG0j4>
